class Animal(object):
    def __init__(self, hungry="yes", name=None, owner=None):
        self.hungry = hungry
        self.name = name
        self.owner = owner

    def noise(self):
        print('errr')

    def _internal_method(self):
        print("hard to find")


    def set_owner(self,newOwner):
        self.__owner= newOwner
        return

    def get_owner(self):
        return self.__owner

    def set_name(self,newName):
        self.__name= newName
        return

    def get_name(self):
        return self.__name

    def noise(self):
        print('errr')
        return

    def __hiddenmethod(self):
        print("hard to find")


def main():
    dog = Animal()    
    dog.owner = 'Sue1'
    print(dog.owner)
    dog.noise()

def main2():
    dog = Animal()    
    dog.set_owner('Sue2')
    print (dog.get_owner())
    dog.noise()

if  __name__ =='__main__':
    main2()